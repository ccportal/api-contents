const express = require('express');
const app = express();
const mongoose = require('mongoose')
const groupRoutes = require('./routes/group.js');
const topicRoutes = require('./routes/topic.js');
const bodyParser = require('body-parser');

mongoose.connect('mongodb://mongo:27017/test?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch((error) => console.log(error));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use(bodyParser.json());

app.use('/groups', groupRoutes);

app.use('/topics', topicRoutes)


module.exports = app;