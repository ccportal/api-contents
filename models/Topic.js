const mongoose = require('mongoose');

const topicSchema = mongoose.Schema({
  group_name: { type: String, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true },
  questions: { type: Object, required: false}
});

module.exports = mongoose.model('Topic', topicSchema);