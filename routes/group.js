const express = require('express');
const Group = require("../models/Group")

const router = express.Router();

router.post('/add', (req, res, next) => {
    const group = new Group({
        name: req.body.name,
        description: req.body.description,
        full_description: req.body.full_description
    });
    group.save().then(
        () => {
            res.status(201).json({
                message: "Group created successfully!"
            }).catch(
                (error) => {
                  res.status(400).json({
                    error: error
                  });
                }
              );
    });
});

router.put('/update', (req, res, next) => {
    Group.updateOne(
        { name: req.body.old_name },
        { $set: {
            name: req.body.name,
            description: req.body.description,
            full_description: req.body.full_description
        }}
    ).then(() => res.status(200).json({ message: 'Groupe mis à jour !'}))
    .catch(error => res.status(400).json({ error }));
});

router.delete('/delete', (req, res, next) => {
    Group.deleteOne(
        { name: req.body.name }
    ).then(() => res.status(200).json({ message: 'Groupe supprimé !'}))
    .catch(error => res.status(400).json({ error }));
});

router.get('/:id', (req, res, next) => {
    Group.findById(req.params.id).then(
        (group) => {
            res.status(200).json(group);
        }
    ).catch(
        (error) => {res.status(404).json(error);}
    );
});

router.get('/', (req, res, next) => {
    Group.find().then(
        (groups) => {
            res.status(200).json(groups);
        }
    ).catch(
        (error) => {
            res.status(404).json(error);
        }
    );
});

module.exports = router;