const express = require('express');
const Topic = require("../models/Topic")

const router = express.Router();

router.post('/add', (req, res, next) => {
    const topic = new Topic({
        group_name: req.body.group_name,
        name: req.body.name,
        description: req.body.description,
    });
    topic.save()
    .then(() => {res.status(201).json({
                message: "Topic created successfully!"
                    })})
    .catch((error) => {res.status(400).json({
                    error: error
                  })})
});

router.put('/update', (req, res, next) => {
    Topic.updateOne(
        { name: req.body.old_name },
        { $set: {
            name: req.body.name,
            description: req.body.description
        } }
    ).then(() => res.status(200).json({ message: 'Sujet mis à jour !'}))
    .catch(error => res.status(400).json({ error }));
});

router.delete('/delete', (req, res, next) => {
    Topic.deleteOne(
        { name: req.body.name }
    ).then(() => res.status(200).json({ message: 'Sujet supprimé !'}))
    .catch(error => res.status(400).json({ error }));
});


router.get('/:group', (req, res, next) => {
    Topic.find({group_name:req.params.group}).then(
        (topics) => {
            res.status(200).json(topics);
        }
    ).catch(
        (error) => {res.status(404).json(error);}
    );
});

router.get('/', (req, res, next) => {
    Topic.find().then(
        (topics) => {
            res.status(200).json(topics);
        }
    ).catch(
        (error) => {
            res.status(404).json(error);
        }
    );
});

router.post('/add/question/', (req, res, next) => {
    const question = {
        content: req.body.content,
    };
    Topic.updateOne(
        { name: req.body.topic_name },
        { $push: { questions: question } }
    ).then(() => res.status(201).json({ message: 'Question ajoutée !'}))
    .catch(error => res.status(400).json({ error }));
})



router.put('/update/question/', (req, res, next) => {
    Topic.updateOne(
        { name: req.body.topic_name },
        { $set: { "questions.$[element].content": req.body.content }},
        { arrayFilters: [{"element.content": req.body.old_content}]}
    ).then(() => res.status(200).json({ message: 'Question mise à jour !'}))
    .catch(error => res.status(400).json({ error }));
})

router.delete('/delete/question/', (req, res, next) => {
    Topic.updateOne(
        { name: req.body.topic_name },
        { $pull: { "questions": {"content": req.body.content }}}
    ).then(() => res.status(200).json({ message: 'Question supprimée !'}))
    .catch(error => res.status(400).json({ error }));
})

router.post('/add/point/', (req, res, next) => {
    const point = {
        name: req.body.name,
        description: req.body.description
    };
    Topic.updateOne(
        { name: req.body.topic_name },
        { $push: { "questions.$[element].points" : point } },
        { arrayFilters: [{"element.content": req.body.question_name}] }
    ).then(() => res.status(201).json({ message: 'Point ajouté !'}))
    .catch(error => res.status(400).json({ error }));
})

router.put('/update/point/', (req, res, next) => {
    Topic.updateOne(
        { name: req.body.topic_name },
        { $set: { "questions.$[element1].points.$[element2].name" : req.body.name,
                  "questions.$[element1].points.$[element2].description": req.body.description } },
        { arrayFilters: [{"element1.content": req.body.content},
                         {"element2.name": req.body.old_name}] }
    ).then(() => res.status(200).json({ message: 'Point mis à jour !'}))
    .catch(error => res.status(400).json({ error }));
})

router.delete('/delete/point/', (req, res, next) => {
    Topic.updateOne(
        { name: req.body.topic_name },
        { $pull: { "questions.$[element1].points" : {"name": req.body.name }}},
        { arrayFilters: [{"element1.content": req.body.content}] }
    ).then(() => res.status(200).json({ message: 'Point supprimé!'}))
    .catch(error => res.status(400).json({ error }));
})


module.exports = router;